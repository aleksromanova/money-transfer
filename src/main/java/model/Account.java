package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * User account
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    private String id;
    private BigDecimal balance;
}
