package http.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


/**
 * Description of http request for money transfer
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MoneyTransferRequest {
    private String accountFrom;
    private String accountTo;
    private BigDecimal value;
}
