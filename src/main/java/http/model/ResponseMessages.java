package http.model;

public class ResponseMessages {
    public static final String SUCCESS = "Transfer successful";
    public static final String SAME_ACCOUNT = "Can't transfer money between same account";
    public static final String ACCOUNT_NOT_EXISTS = "Account %s doesn't exist";
    public static final String NO_MONEY = "Not enough money";
}
