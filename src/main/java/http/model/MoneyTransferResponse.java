package http.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Description for http response for money transfer
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MoneyTransferResponse {
    private String message;
}
