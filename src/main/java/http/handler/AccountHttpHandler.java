package http.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Splitter;
import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import exception.AccountCreationException;
import lombok.AllArgsConstructor;
import model.Account;
import storage.Storage;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Map;

/**
 * Http handler for get account function
 */
@AllArgsConstructor
public class AccountHttpHandler implements HttpHandler {
    private Storage storage;

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        if (HttpMethod.GET.equals(httpExchange.getRequestMethod())) {
            getAccount(httpExchange);
        } else if (HttpMethod.POST.equals(httpExchange.getRequestMethod())) {
            createAccount(httpExchange);
        } else {
            httpExchange.sendResponseHeaders(Response.Status.METHOD_NOT_ALLOWED.getStatusCode(), -1);
        }
    }

    private void getAccount(HttpExchange httpExchange) throws IOException {
        URI uri = httpExchange.getRequestURI();
        String query = uri.getQuery();
        if (query == null || !query.contains("=")) {
            httpExchange.sendResponseHeaders(Response.Status.BAD_REQUEST.getStatusCode(), -1);
            httpExchange.close();
            return;
        }
        Map<String, String> queryParams = Splitter.on('&').trimResults().withKeyValueSeparator("=").split(query);
        String accountId = queryParams.get("accountId");
        if (accountId == null) {
            httpExchange.sendResponseHeaders(Response.Status.BAD_REQUEST.getStatusCode(), -1);
            httpExchange.close();
            return;
        }
        Account account = storage.getAccount(accountId);
        if (account == null) {
            httpExchange.sendResponseHeaders(Response.Status.NOT_FOUND.getStatusCode(), -1);
            httpExchange.close();
            return;
        }
        httpExchange.sendResponseHeaders(Response.Status.OK.getStatusCode(), 0);
        OutputStream responseBody = httpExchange.getResponseBody();
        Gson gson = new Gson();
        String responseString = gson.toJson(account);
        responseBody.write(responseString.getBytes());
        responseBody.close();
        httpExchange.close();
    }

    private void createAccount(HttpExchange httpExchange) throws IOException {
        InputStream inputStream = httpExchange.getRequestBody();
        ObjectMapper objectMapper = new ObjectMapper();
        Account account = objectMapper.readValue(inputStream, Account.class);
        try {
            storage.addAccount(account);
            httpExchange.sendResponseHeaders(Response.Status.CREATED.getStatusCode(), 0);
        } catch (AccountCreationException e) {
            httpExchange.sendResponseHeaders(Response.Status.CONFLICT.getStatusCode(), -1);
        }
    }
}
