package http.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import exception.TransferException;
import http.model.MoneyTransferRequest;
import http.model.MoneyTransferResponse;
import http.model.ResponseMessages;
import javafx.util.Pair;
import lombok.AllArgsConstructor;
import storage.Storage;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Http handler for money transfer function
 */
@AllArgsConstructor
public class MoneyTransferHttpHandler implements HttpHandler {
    private Storage storage;

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        if (HttpMethod.POST.equals(httpExchange.getRequestMethod())) {
            InputStream inputStream = httpExchange.getRequestBody();
            ObjectMapper objectMapper = new ObjectMapper();
            MoneyTransferRequest request = objectMapper.readValue(inputStream, MoneyTransferRequest.class);
            Pair<Response.Status, MoneyTransferResponse> response = transferMoney(request);
            httpExchange.sendResponseHeaders(response.getKey().getStatusCode(), 0);
            OutputStream responseBody = httpExchange.getResponseBody();
            Gson gson = new Gson();
            String responseString = gson.toJson(response.getValue());
            responseBody.write(responseString.getBytes());
            responseBody.close();
        } else {
            httpExchange.sendResponseHeaders(Response.Status.METHOD_NOT_ALLOWED.getStatusCode(), -1);
        }
        httpExchange.close();
    }

    private Pair<Response.Status, MoneyTransferResponse> transferMoney(MoneyTransferRequest request) {
        try {
            storage.transferMoney(request.getAccountFrom(), request.getAccountTo(), request.getValue());
            return new Pair<>(Response.Status.OK, new MoneyTransferResponse(ResponseMessages.SUCCESS));
        } catch (TransferException e) {
            return new Pair<>(Response.Status.BAD_REQUEST, new MoneyTransferResponse(e.getMessage()));
        }

    }
}
