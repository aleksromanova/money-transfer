package http;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Config parameters for http server
 */
@Data
@NoArgsConstructor
public class HttpServerConfig {
    private int port = 8000;
    private int threadCount = 20;
}
