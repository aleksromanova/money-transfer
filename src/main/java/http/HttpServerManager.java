package http;

import com.sun.net.httpserver.HttpServer;
import http.handler.AccountHttpHandler;
import http.handler.MoneyTransferHttpHandler;
import storage.Storage;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * HttpServer manager
 */
public class HttpServerManager {
    private HttpServer server;
    private Storage storage;

    public HttpServerManager(Storage storage) {
        this.storage = storage;
    }

    /**
     * Starts http server
     *
     * @param config config parameters for server
     */
    public void startServer(HttpServerConfig config) {
        try {
            server = HttpServer.create(new InetSocketAddress(config.getPort()), 0);
            server.createContext("/api/transfer", new MoneyTransferHttpHandler(storage));
            server.createContext("/api/account", new AccountHttpHandler(storage));
            server.setExecutor(Executors.newFixedThreadPool(config.getThreadCount()));
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Stops server
     */
    public void stopServer() {
        server.stop(0);
    }
}
