package storage;

import exception.AccountCreationException;
import exception.TransferException;
import http.model.ResponseMessages;
import model.Account;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Storage for accounts data
 */
public class Storage {

    private Map<String, Account> accounts = new ConcurrentHashMap<>();

    /**
     * Add account to storage
     *
     * @param account new account
     * @throws AccountCreationException in case problems while trying to add account
     */
    public void addAccount(Account account) throws AccountCreationException {
        if (accounts.containsKey(account.getId())) {
            throw new AccountCreationException("Account already exists");
        } else {
            accounts.put(account.getId(), account);
        }
    }

    /**
     * Transfer money between accounts
     *
     * @param accountFrom id of source account
     * @param accountTo   id of destination account
     * @param value       value of transfering balance
     * @throws TransferException in case transfer can\t be processed
     */
    public void transferMoney(String accountFrom, String accountTo, BigDecimal value) throws TransferException {
        if (accountFrom.equals(accountTo)) {
            throw new TransferException(ResponseMessages.SAME_ACCOUNT);
        }
        String firstAccount = accountFrom.compareTo(accountTo) > 0 ? accountFrom : accountTo;
        String secondAccount = accountTo.compareTo(accountFrom) > 0 ? accountFrom : accountTo;

        if (!accounts.containsKey(accountFrom)) {
            throw new TransferException(String.format(ResponseMessages.ACCOUNT_NOT_EXISTS, accountFrom));
        }

        if (!accounts.containsKey(accountTo)) {
            throw new TransferException(String.format(ResponseMessages.ACCOUNT_NOT_EXISTS, accountTo));
        }

        synchronized (accounts.get(firstAccount)) {
            synchronized (accounts.get(secondAccount)) {
                BigDecimal valueFrom = accounts.get(accountFrom).getBalance();
                BigDecimal valueTo = accounts.get(accountTo).getBalance();
                if (valueFrom == null || valueFrom.compareTo(value) < 0) {
                    throw new TransferException(ResponseMessages.NO_MONEY);
                }
                accounts.get(accountFrom).setBalance(valueFrom.subtract(value));
                accounts.get(accountTo).setBalance(valueTo.add(value));
            }
        }
    }

    /**
     * Get account from storage
     *
     * @param accountId id of the account
     * @return account
     */
    public Account getAccount(String accountId) {
        return accounts.get(accountId);
    }

    public Map<String, Account> getAccounts() {
        return Collections.unmodifiableMap(accounts);
    }
}
