import http.HttpServerConfig;
import http.HttpServerManager;
import storage.Storage;

/**
 * Main class, starts application
 */
public class MoneyTransfer {
    private static final int defaultPort = 8000;

    public static void main(String... args) {
        Storage storage = new Storage();
        HttpServerManager serverManager = new HttpServerManager(storage);
        serverManager.startServer(new HttpServerConfig());
    }

}
