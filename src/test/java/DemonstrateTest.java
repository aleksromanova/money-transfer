import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import http.HttpServerConfig;
import http.HttpServerManager;
import http.model.MoneyTransferRequest;
import http.model.MoneyTransferResponse;
import http.model.ResponseMessages;
import model.Account;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import storage.Storage;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;

class DemonstrateTest {
    private Gson gson = new Gson();
    private HttpServerManager serverManager;
    private Storage storage;
    private ObjectMapper objectMapper = new ObjectMapper();


    @BeforeEach
    void before() {
        storage = new Storage();
        serverManager = new HttpServerManager(storage);
        serverManager.startServer(new HttpServerConfig());
    }

    @Test
    @DisplayName("Simple account creation")
    void accountCreationTest() throws Exception {
        String accountId = "123";
        HttpPost request = generateAccountCreateRequest(accountId, new BigDecimal(0));
        HttpResponse response = executeRequest(request);

        Assertions.assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatusLine().getStatusCode(), "Wrong status code!");
        Assertions.assertNotNull(storage.getAccount(accountId), "No account in storage!");
    }

    @Test
    @DisplayName("Duplicate account creation")
    void duplicateAccountCreationTest() throws Exception {
        HttpPost request = generateAccountCreateRequest("123", new BigDecimal(0));
        executeRequest(request);
        HttpResponse response = executeRequest(request);

        Assertions.assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatusLine().getStatusCode(), "Wrong status code!");
    }

    @Test
    @DisplayName("Positive money transfer")
    void moneyTransferTest() throws Exception {
        String accountIdFrom = "123";
        String accountIdTo = "456";
        BigDecimal balanceFrom = new BigDecimal(100);
        BigDecimal balanceTo = new BigDecimal(0);
        BigDecimal transferValue = new BigDecimal(10);

        HttpPost creationRequest = generateAccountCreateRequest(accountIdFrom, balanceFrom);
        executeRequest(creationRequest);

        creationRequest = generateAccountCreateRequest(accountIdTo, balanceTo);
        executeRequest(creationRequest);

        HttpPost transferRequest = generateTransferRequest(accountIdFrom, accountIdTo, transferValue);
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpResponse response = httpClient.execute(transferRequest);
        MoneyTransferResponse transferResponse = objectMapper.readValue(response.getEntity().getContent(), MoneyTransferResponse.class);
        Assertions.assertEquals(Response.Status.OK.getStatusCode(), response.getStatusLine().getStatusCode(), "Wrong status code!");
        Assertions.assertEquals(new BigDecimal(90), storage.getAccount(accountIdFrom).getBalance(), "Wrong result balance!");
        Assertions.assertEquals(ResponseMessages.SUCCESS, transferResponse.getMessage(), "Wrong result message!");
        httpClient.close();
    }

    @Test
    @DisplayName("Not existing account money transfer")
    void NoAccountMoneyTransferTest() throws Exception {
        String accountIdFrom = "123";
        String accountIdTo = "456";
        BigDecimal transferValue = new BigDecimal(10);

        HttpPost transferRequest = generateTransferRequest(accountIdFrom, accountIdTo, transferValue);
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpResponse response = httpClient.execute(transferRequest);
        MoneyTransferResponse transferResponse = objectMapper.readValue(response.getEntity().getContent(), MoneyTransferResponse.class);
        Assertions.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatusLine().getStatusCode(), "Wrong status code!");
        Assertions.assertEquals(String.format(ResponseMessages.ACCOUNT_NOT_EXISTS, accountIdFrom), transferResponse.getMessage(), "Wrong result message!");
        httpClient.close();
    }

    @Test
    @DisplayName("Not enough money transfer")
    void NoEnoughMoneyTransferTest() throws Exception {
        String accountIdFrom = "123";
        String accountIdTo = "456";
        BigDecimal balanceFrom = new BigDecimal(0);
        BigDecimal balanceTo = new BigDecimal(0);
        BigDecimal transferValue = new BigDecimal(10);

        HttpPost creationRequest = generateAccountCreateRequest(accountIdFrom, balanceFrom);
        executeRequest(creationRequest);

        creationRequest = generateAccountCreateRequest(accountIdTo, balanceTo);
        executeRequest(creationRequest);

        HttpPost transferRequest = generateTransferRequest(accountIdFrom, accountIdTo, transferValue);
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpResponse response = httpClient.execute(transferRequest);
        MoneyTransferResponse transferResponse = objectMapper.readValue(response.getEntity().getContent(), MoneyTransferResponse.class);
        Assertions.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatusLine().getStatusCode(), "Wrong status code!");
        Assertions.assertEquals(ResponseMessages.NO_MONEY, transferResponse.getMessage(), "Wrong result message!");
        httpClient.close();
    }

    @Test
    @DisplayName("Same account money transfer")
    void SameAccountMoneyTransferTest() throws Exception {
        String accountId = "123";
        BigDecimal balance = new BigDecimal(0);
        BigDecimal transferValue = new BigDecimal(10);

        HttpPost creationRequest = generateAccountCreateRequest(accountId, balance);
        executeRequest(creationRequest);

        HttpPost transferRequest = generateTransferRequest(accountId, accountId, transferValue);
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpResponse response = httpClient.execute(transferRequest);
        MoneyTransferResponse transferResponse = objectMapper.readValue(response.getEntity().getContent(), MoneyTransferResponse.class);
        Assertions.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatusLine().getStatusCode(), "Wrong status code!");
        Assertions.assertEquals(ResponseMessages.SAME_ACCOUNT, transferResponse.getMessage(), "Wrong result message!");
        httpClient.close();
    }

    @Test
    @DisplayName("Simple get account test")
    void getAccountTest() throws Exception {
        String accountId = "123";
        HttpPost request = generateAccountCreateRequest(accountId, new BigDecimal(0));
        executeRequest(request);

        HttpGet getRequest = generateGetAccountRequest(accountId);
        HttpResponse response = executeRequest(getRequest);

        Assertions.assertEquals(Response.Status.OK.getStatusCode(), response.getStatusLine().getStatusCode(), "Wrong status code!");
    }

    private HttpPost generateAccountCreateRequest(String accountId, BigDecimal accountBalance) {
        HttpPost httpPost = new HttpPost("http://localhost:8000/api/account");
        Account account = new Account(accountId, accountBalance);
        StringEntity stringEntity = new StringEntity(gson.toJson(account), StandardCharsets.UTF_8);
        httpPost.setEntity(stringEntity);
        return httpPost;
    }

    private HttpPost generateTransferRequest(String accountIdFrom, String accountIdTo, BigDecimal value) {
        HttpPost httpPost = new HttpPost("http://localhost:8000/api/transfer");
        MoneyTransferRequest request = new MoneyTransferRequest(accountIdFrom, accountIdTo, value);
        StringEntity stringEntity = new StringEntity(gson.toJson(request), StandardCharsets.UTF_8);
        httpPost.setEntity(stringEntity);
        return httpPost;
    }

    private HttpGet generateGetAccountRequest(String accountId) {
        return new HttpGet("http://localhost:8000/api/account?accountId=" + accountId);
    }

    private HttpResponse executeRequest(HttpRequestBase request) throws Exception {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpResponse response = httpClient.execute(request);
        httpClient.close();
        return response;
    }

    @AfterEach
    void after() {
        serverManager.stopServer();
    }


}
