package storage;

import exception.AccountCreationException;
import exception.TransferException;
import model.Account;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class StorageTest {
    private Storage storage;

    @BeforeEach
    void before() {
        storage = new Storage();
    }

    @Test
    void addAccountTest() throws Exception {
        String accountId = "123";
        BigDecimal balance = new BigDecimal(100);
        Account account = new Account(accountId, balance);

        storage.addAccount(account);
        Assertions.assertEquals(account, storage.getAccounts().get(accountId));
    }

    @Test
    void addExistingAccountTest() throws Exception {
        String accountId = "123";
        BigDecimal balance = new BigDecimal(100);
        Account account = new Account(accountId, balance);

        storage.addAccount(account);
        Assertions.assertThrows(AccountCreationException.class, () -> storage.addAccount(account));
    }

    @Test
    void moneyTransferTest() throws Exception {
        String accountId1 = "123";
        BigDecimal balance1 = new BigDecimal(100);
        Account account1 = new Account(accountId1, balance1);

        String accountId2 = "345";
        BigDecimal balance2 = new BigDecimal(100);
        Account account2 = new Account(accountId2, balance2);

        storage.addAccount(account1);
        storage.addAccount(account2);

        storage.transferMoney(accountId1, accountId2, new BigDecimal(10));

        Assertions.assertEquals(new BigDecimal(90), storage.getAccounts().get(accountId1).getBalance());
        Assertions.assertEquals(new BigDecimal(110), storage.getAccounts().get(accountId2).getBalance());
    }

    @Test
    void moneyTransferNoAccountTest() {
        String accountId1 = "123";
        String accountId2 = "345";

        Assertions.assertThrows(TransferException.class, () -> storage.transferMoney(accountId1, accountId2, new BigDecimal(10)));
    }

    @Test
    void moneyTransferNotEnoughMoneyTest() throws Exception {
        String accountId1 = "123";
        BigDecimal balance1 = new BigDecimal(100);
        Account account1 = new Account(accountId1, balance1);

        String accountId2 = "345";
        BigDecimal balance2 = new BigDecimal(100);
        Account account2 = new Account(accountId2, balance2);

        storage.addAccount(account1);
        storage.addAccount(account2);

        Assertions.assertThrows(TransferException.class, () -> storage.transferMoney(accountId1, accountId2, new BigDecimal(1000)));
    }

    @Test
    void moneyTransferSameAccountTest() throws Exception {
        String accountId = "123";
        BigDecimal balance = new BigDecimal(100);
        Account account = new Account(accountId, balance);

        storage.addAccount(account);

        Assertions.assertThrows(TransferException.class, () -> storage.transferMoney(accountId, accountId, new BigDecimal(1000)));
    }
}
