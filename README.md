# Create account
Create account in the storage.

**URL** : `/api/account/`

**Method** : `POST`

**Data constraints**
```json
{
    "id": "[string]",
    "balance": "[BigDecimal]"
}
```

## Success Response

**Code** : `200 OK`

## Error Response

**Condition** : If account is allready exists.

**Code** : `409 CONFLICT`


# Get account info
Create account in the storage.

**URL** : `/api/account/`

**URL Parameters** : `accountId=[string]` where `accountId` is the ID of the Account
server.

**Method** : `GET`

## Success Response

**Code** : `200 OK`

## Error Response

**Condition** : If account doesn't exists.

**Code** : `404 NOT FOUND`

**Condition** : If request URL is incorrect.

**Code** : `400 BAD REQUEST`

# Transfer money between accounts 
Transfer money from one account to another.

**URL** : `/api/transfer`

**Method** : `POST`

**Data constraints**
```json
{
    "accountFrom": "[string]",
    "accountTo": "[string]",
    "value": "[BigDecimal]"
}
```

## Success Response

**Code** : `200 OK`

**Content** :

```json
{"message": "Transfer successful"}
```

## Error Response

**Condition** : If transfer can't be completed.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{"message": "Account %s doesn't exist"}
```
```json
{"message": "Can't transfer money between same account"}
```
```json
{"message": "Not enough money"}
```

